import styled from '@emotion/styled';
import { Box } from '@mui/material';

export const DatePickerWrap = styled(Box)`
  display: flex;
  align-items: center;
  width: 100%;
`;
